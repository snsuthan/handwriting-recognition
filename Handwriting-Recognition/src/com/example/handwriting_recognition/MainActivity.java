package com.example.handwriting_recognition;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout; 
import android.widget.Toast;

import com.TamilLanguage.*;

public class MainActivity extends Activity {

 View mView; 
 private Paint mPaint;
 
 boolean request_pros_alive;
 
 long now_time;
 
 String target_language="ta";
 
 private Path path;
 private Bitmap mBitmap;
 private Canvas mCanvas;
 
 private ArrayList<WriteAreaData> draw_graphics;
 public JSONArray inkJsonArray;
 
 LinearLayout write_ara_layout;
 
 public ArrayList<JSONArray> waiting_request_ink_list;
 
 
 HorizontalListView result_listview;
 
 @Override
 protected void onCreate(Bundle savedInstanceState) {
  super.onCreate(savedInstanceState);
  setContentView(R.layout.activity_main);
  
  write_ara_layout = (LinearLayout) findViewById(R.id.writing_area);
  
  result_listview = (HorizontalListView) findViewById(R.id.listview);
  
   
  
  
  Button clear_btn=(Button) findViewById(R.id.clear_btn);
  clear_btn.setOnClickListener(new View.OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		// TODO Auto-generated method stub
		clearWriteArea();
	}
});
  
  init(); 
  
 }

 public void init() {
	 
	 mView = new DrawingView(this);
	  
	  write_ara_layout.addView(mView, new LayoutParams(  
	    LinearLayout.LayoutParams.MATCH_PARENT,  
	    LinearLayout.LayoutParams.MATCH_PARENT)); 
	 
  mPaint = new Paint();
  mPaint.setDither(true);
  mPaint.setColor(0xFFFFFF00);
  mPaint.setStyle(Paint.Style.STROKE);
  mPaint.setStrokeJoin(Paint.Join.ROUND);
  mPaint.setStrokeCap(Paint.Cap.ROUND);
  mPaint.setStrokeWidth(3);
  
  
  waiting_request_ink_list=new ArrayList<JSONArray>();
  request_pros_alive=false;
  result_listview.setVisibility(View.INVISIBLE);
  now_time=0;
  
  
 }
 
 public void clearWriteArea(){
	 
	 if(!request_pros_alive){
		
		 mBitmap.eraseColor(Color.TRANSPARENT);
		 path.reset();
		 mView.invalidate();
		 
		 init(); 
		
	 }else{
		 
		 Toast.makeText(getBaseContext(), "Wait Until past request finish....", Toast.LENGTH_SHORT).show();
	 }
	 
	
	
 }
 
 
public ArrayList<String> unicodeToBamini(JSONArray result_array){
	 
	 
	ArrayList<String> result_text=new ArrayList<String>();
	 for (int i = 0; i < result_array.length(); i++) {
		 try {
			result_text.add(TamilUtil.convertToTamil(TamilUtil.TSCII, result_array.getString(i)));
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	 
	
	 
	 return result_text;
 }


 class DrawingView extends View {
  

  public DrawingView(Context context) {
   super(context);
   path = new Path();
   
   float density = context.getResources().getDisplayMetrics().density;
   mBitmap = Bitmap.createBitmap((int) (600*density), (int) (300*density), Bitmap.Config.ARGB_8888);
   mCanvas = new Canvas(mBitmap);
   
   draw_graphics = new ArrayList<WriteAreaData>();
   inkJsonArray=new JSONArray();
   
   
   this.setBackgroundColor(Color.TRANSPARENT);
  }

  
  @Override
  public boolean onTouchEvent(MotionEvent event) {
	  
   WriteAreaData pp = new WriteAreaData();
   mCanvas.drawPath(path, mPaint);
   if (event.getAction() == MotionEvent.ACTION_DOWN) {
	   
	   draw_graphics.clear();
	   
    path.moveTo(event.getX(), event.getY());
    path.lineTo(event.getX(), event.getY());
    
    
   } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
    path.lineTo(event.getX(), event.getY());
    pp.setPath(path);
    pp.setmPaint(mPaint);
    
    if(now_time==0){
    	now_time=System.currentTimeMillis();
    }
    
    pp.setTime(System.currentTimeMillis()-now_time);
    pp.setX((int)event.getX());
    pp.setY((int)event.getY());
    
    draw_graphics.add(pp);
    
  
   }else if (event.getAction() == MotionEvent.ACTION_UP){
	   try {
		   
		   if(inkJsonArray.length()>0){
				
				try {
					inkJsonArray.join(",");
				} catch (JSONException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
		   inkJsonArray.put(createJSONArray(draw_graphics));
		   
		   waiting_request_ink_list.add(inkJsonArray);
		   
		  
		   
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	   
	   if(!request_pros_alive){
		  new GetResults().execute(); 
	   }
	   
	  
   }
   postInvalidate();
   return true;
  }

  @Override
  protected void onDraw(Canvas canvas) {
   super.onDraw(canvas);
   if (draw_graphics.size()> 0) {
    canvas.drawPath(
      draw_graphics.get(draw_graphics.size()- 1).getPath(),
      draw_graphics.get(draw_graphics.size()- 1).getmPaint());
    
    
   }
  }
  
  public JSONArray createJSONArray(ArrayList<WriteAreaData> _graphics) throws JSONException{
	  
	  JSONArray xArray = new JSONArray();
	  JSONArray yArray = new JSONArray();
	  JSONArray timeArray = new JSONArray();
	  
	  
	  JSONArray dataArray = new JSONArray();
	  
	  for (WriteAreaData pathWithPaint : _graphics) {
		  xArray.put(pathWithPaint.getX());
		  yArray.put(pathWithPaint.getY());
		  timeArray.put(pathWithPaint.getTime());
		
	      }
	 
	  dataArray.put(xArray);
	  dataArray.join(",");
	  
	  dataArray.put(yArray);
	  dataArray.join(",");
	  
	  dataArray.put(timeArray);
	  
	 
	  
	return dataArray;
	
	  
      }
  
  
  
  
 }
 
 
 
 
 public JSONObject getHttpRequestBody(){
		
		JSONObject all_request_body = new JSONObject();
		
		
		
		try {
			
			JSONObject writing_areaobject = new JSONObject();
			JSONObject requestsobject = new JSONObject();
			
			float density = getApplicationContext().getResources().getDisplayMetrics().density;
			
			writing_areaobject.put("writing_area_width",(int) (600*density));
			writing_areaobject.put("writing_area_height",(int) (300*density));
			
			requestsobject.put("writing_guide",writing_areaobject);
			requestsobject.put("pre_context", "");
			requestsobject.put("max_num_results",10);
			requestsobject.put("max_completions",0);
			requestsobject.put("ink",waiting_request_ink_list.get(0));
			
			
			
			all_request_body.put("app_version",0.4);
			all_request_body.put("api_level", "537.36");
			all_request_body.put("input_type", 0);
			all_request_body.put("options", "enable_pre_space");
			all_request_body.put("requests", new JSONArray().put(requestsobject));
         
			
			
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return all_request_body;
		
	}
 
 
 
 
 
 
 
 
 class GetResults extends AsyncTask<String, String, String> {

		
	 private  String result;
	 
	 
	 boolean net_status=true;


		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			request_pros_alive=true;
			
		}

		
		protected String doInBackground(String... args) {
			
			try {
				
				
	            HttpClient httpclient = new DefaultHttpClient();
	            
	            HttpPost httpPost = new HttpPost("https://inputtools.google.com/request?itc="+target_language+"-t-i0-handwrit");
				
				
				StringEntity se = new StringEntity(new String(getHttpRequestBody().toString()));
				
				httpPost.setEntity(se);
				 
	            
	            httpPost.setHeader("Accept", "*/*");
	            httpPost.setHeader("Content-type", "application/json");
	 
	            InputStream inputStream = null ;
	            
	            if(((ConnectivityManager) getBaseContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null){
           			
	            	 HttpResponse httpResponse = httpclient.execute(httpPost);
	            	
	            	inputStream = httpResponse.getEntity().getContent();
	            	
        		 }else{
        			 net_status=false;
        		 }
	            
	            
	            
	           
	 
	            
	            
	 
	            // 10. convert inputstream to string
	            if(inputStream != null)
	                {
	            	BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
	                String line = "\n";
	                result = "";
	                while((line = bufferedReader.readLine()) != null)
	                    result +=line;
	         
	                inputStream.close();
	                
	                
	                if(!result.equals("")){
						JSONArray jsonArray=new JSONArray(result);
						if(jsonArray.get(0).equals("SUCCESS")){
							
							
							waiting_request_ink_list.remove(0);
							
							
							
							
							if (jsonArray.getJSONArray(1).getJSONArray(0).getJSONArray(1) != null) { 
								
								final ResultViewAdapter viewAdapter=new ResultViewAdapter(getBaseContext(), unicodeToBamini(jsonArray.getJSONArray(1).getJSONArray(0).getJSONArray(1)));
							    result_listview.post(new Runnable() {
									
									@Override
									public void run() {
										// TODO Auto-generated method stub
										result_listview.setVisibility(View.VISIBLE);
										result_listview.setAdapter(viewAdapter);
										
									}
								});
							
							} 
							
							
							
						}
					}
	                }
				
				
				
				
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalStateException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
			
			return null;
		}

		
		protected void onPostExecute(String file_url) {
			
			if(net_status){
				
				if(waiting_request_ink_list.size()>0){
					new GetResults().execute(); 
					
				}else{
					request_pros_alive=false;
				}
				
			}else{
				request_pros_alive=false;
				Toast.makeText(getApplicationContext(),"No Network connection", Toast.LENGTH_SHORT).show();
			}
			
			
		}

	}
}


