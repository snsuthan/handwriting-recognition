package com.example.handwriting_recognition;

import android.graphics.Paint;
import android.graphics.Path;

public class WriteAreaData {
 private Path path;
 
 public int x,y;
 
 long time;
 

 public int getX() {
	return x;
}

public void setX(int x) {
	this.x = x;
}

public int getY() {
	return y;
}

public void setY(int y) {
	this.y = y;
}

public long getTime() {
	return time;
}

public void setTime(long time) {
	this.time = time;
}

public Path getPath() {
  return path;
 }

 public void setPath(Path path) {
  this.path = path;
 }

 private Paint mPaint;

 public Paint getmPaint() {
  return mPaint;
 }

 public void setmPaint(Paint mPaint) {
  this.mPaint = mPaint;
 }
}


