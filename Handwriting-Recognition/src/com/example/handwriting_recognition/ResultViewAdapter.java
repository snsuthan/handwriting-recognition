package com.example.handwriting_recognition;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class ResultViewAdapter extends BaseAdapter {
	
	Context context;
	ArrayList<String> wordList;

	public ResultViewAdapter(Context context,ArrayList<String> wordList) {
		// TODO Auto-generated constructor stub
		this.context=context;
		this.wordList=wordList;
	}
	
	
	
	
	@Override
	public int getCount() {
		return wordList.size();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View retval = LayoutInflater.from(parent.getContext()).inflate(R.layout.viewitem, null);
		TextView view_result_text = (TextView) retval.findViewById(R.id.view_result_text);
		Typeface tfTam = Typeface.createFromAsset(context.getAssets(),"fonts/TneriTSC.ttf");
		view_result_text.setTypeface(tfTam);
		view_result_text.setText(wordList.get(position));
		
		view_result_text.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				Toast.makeText(context,wordList.get(position), Toast.LENGTH_SHORT).show();
				
			}
		});
		
		return retval;
	}

}
